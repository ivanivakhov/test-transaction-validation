<?php

namespace Tests\Unit;

use App\DTOs\RequestDto;
use App\DTOs\TransactionDto;
use App\Services\RequestMoneyValidator;
use PHPUnit\Framework\TestCase;

class RequestMoneyValidatorTest extends TestCase
{
    private function set_test_dto($request, $transaction) {
        $requestDto = new RequestDto();
        $requestDto->amount = $request['amount'];
        $requestDto->currency = $request['currency'];

        $transactionDto = new TransactionDto();
        $transactionDto->amount = $transaction['amount'];
        $transactionDto->currency = $transaction['currency'];
        return ['request' => $requestDto, 'transaction' => $transactionDto];
    }

    public function test_validation_of_lowest_border_returns_true(): void
    {
        $amount_requested = 100;
        $deviation = env('DEVIATION');
        $percent_of_requested = $amount_requested*$deviation/100;
        $amount_transaction = $amount_requested - $percent_of_requested;
        echo "--- lowest border ---\n";
        echo "request amount = $amount_requested\n";
        echo "transaction amount = $amount_transaction\n";
        echo "deviation = $deviation\n";
        $dtos = $this->set_test_dto(['amount' => $amount_requested, 'currency' => 'usd'], ['amount' => $amount_transaction, 'currency' => 'usd']);

        $validator = new RequestMoneyValidator();
        $this->assertEquals(true, $validator->validate($dtos['request'], $dtos['transaction']));
    }


    public function test_validation_of_highest_border_returns_true(): void
    {
        $amount_requested = 100;
        $deviation = env('DEVIATION');
        $percent_of_requested = $amount_requested*$deviation/100;
        $amount_transaction = $amount_requested + $percent_of_requested;
        echo "--- highest border ---\n";
        echo "request amount = $amount_requested\n";
        echo "transaction amount = $amount_transaction\n";
        echo "deviation = $deviation\n";
        $dtos = $this->set_test_dto(['amount' => $amount_requested, 'currency' => 'usd'], ['amount' => $amount_transaction, 'currency' => 'usd']);

        $validator = new RequestMoneyValidator();
        $this->assertEquals(true, $validator->validate($dtos['request'], $dtos['transaction']));
    }

    public function test_validation_of_lowest_border_minus_one_cent_returns_false(): void
    {
        $amount_requested = 100;
        $deviation = env('DEVIATION');
        $percent_of_requested = $amount_requested*$deviation/100;
        $amount_transaction = $amount_requested - $percent_of_requested - 0.01;
        echo "--- lowest border minus 1 cent ---\n";
        echo "request amount = $amount_requested\n";
        echo "transaction amount = $amount_transaction\n";
        echo "deviation = $deviation\n";
        $dtos = $this->set_test_dto(['amount' => $amount_requested, 'currency' => 'usd'], ['amount' => $amount_transaction, 'currency' => 'usd']);

        $validator = new RequestMoneyValidator();
        $this->assertEquals(false, $validator->validate($dtos['request'], $dtos['transaction']));
    }


    public function test_validation_of_highest_border_plus_one_cent_returns_false(): void
    {
        $amount_requested = 100;
        $deviation = env('DEVIATION');
        $percent_of_requested = $amount_requested*$deviation/100;
        $amount_transaction = $amount_requested + $percent_of_requested + 0.01;
        echo "--- highest border + 1 cent ---\n";
        echo "request amount = $amount_requested\n";
        echo "transaction amount = $amount_transaction\n";
        echo "deviation = $deviation\n";
        $dtos = $this->set_test_dto(['amount' => $amount_requested, 'currency' => 'usd'], ['amount' => $amount_transaction, 'currency' => 'usd']);

        $validator = new RequestMoneyValidator();
        $this->assertEquals(false, $validator->validate($dtos['request'], $dtos['transaction']));
    }


    public function test_validation_of_different_currencies_returns_false(): void
    {
        $amount_requested = 100;
        $deviation = env('DEVIATION');
        $percent_of_requested = $amount_requested*$deviation/100;
        $amount_transaction = $amount_requested - $percent_of_requested;
        echo "--- different currencies ---\n";
        $dtos = $this->set_test_dto(['amount' => $amount_requested, 'currency' => 'usd'], ['amount' => $amount_transaction, 'currency' => 'eur']);

        $validator = new RequestMoneyValidator();
        $this->assertEquals(false, $validator->validate($dtos['request'], $dtos['transaction']));
    }

    //this is what you warned me that floats makes troubles. I noticed that there are some better methods to avoid this bug, but for keep it simple I use just round() function

    public function test_validation_of_lowest_border_minus_less_than_one_cent_returns_false(): void
    {
        $amount_requested = 100;
        $deviation = env('DEVIATION');
        $percent_of_requested = $amount_requested*$deviation/100;
        $amount_transaction = $amount_requested - $percent_of_requested - 0.001;
        echo "--- MUST BE FALSE ---\n";
        echo "request amount = $amount_requested\n";
        echo "transaction amount = $amount_transaction\n";
        echo "deviation = $deviation\n";
        $dtos = $this->set_test_dto(['amount' => $amount_requested, 'currency' => 'usd'], ['amount' => $amount_transaction, 'currency' => 'usd']);

        $validator = new RequestMoneyValidator();
        $this->assertEquals(false, $validator->validate($dtos['request'], $dtos['transaction']));
    }


    public function test_validation_of_highest_border_plus_less_than_half_cent_returns_false(): void
    {
        $amount_requested = 100;
        $deviation = env('DEVIATION');
        $percent_of_requested = $amount_requested*$deviation/100;
        $amount_transaction = $amount_requested + $percent_of_requested  + 0.004;
        echo "--- MUST BE FALSE ---\n";
        echo "request amount = $amount_requested\n";
        echo "transaction amount = $amount_transaction\n";
        echo "deviation = $deviation\n";
        $dtos = $this->set_test_dto(['amount' => $amount_requested, 'currency' => 'usd'], ['amount' => $amount_transaction, 'currency' => 'usd']);

        $validator = new RequestMoneyValidator();
        $this->assertEquals(false, $validator->validate($dtos['request'], $dtos['transaction']));
    }
}
