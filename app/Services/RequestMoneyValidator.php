<?php

namespace App\Services;

use App\DTOs\RequestDto as Request;
use App\DTOs\TransactionDto as Transaction;

class RequestMoneyValidator
{
    private function isValidCurrency(string $currency_requested, string $currency_transaction): bool
    {
        if (strtolower($currency_requested) === strtolower($currency_transaction))
            return true;
        return false;
    }

    private function inGivenDeviation(float $amount_requested, float $amount_transaction): bool
    {
        $deviation = env('DEVIATION');
        $percent_of_requested = $amount_requested*$deviation/100;
        if (
            $amount_requested - $percent_of_requested <= $amount_transaction &&
            $amount_requested + $percent_of_requested >= $amount_transaction
        )
            return true;
        return false;
    }

    public function validate(Request $request, Transaction $transaction): bool
    {
        if (
            !$this->isValidCurrency($request->currency, $transaction->currency) ||
            !$this->inGivenDeviation(round($request->amount, 2), round($transaction->amount, 2))
        )
            return false;

        return true;
    }


}
