<?php

namespace App\Http\Controllers;

use App\DTOs\RequestDto;
use App\DTOs\TransactionDto;
use App\Services\RequestMoneyValidator;

class ValidateController extends Controller
{

    public function __construct(){}

    public function validateRequest() {
        //it comes from http request or so..
        $request = ['amount' => 100, 'currency' => 'usd'];
        $transaction = ['amount' => 89.994, 'currency' => 'usd'];

        $requestDto = new RequestDto();
        $requestDto->amount = $request['amount'];
        $requestDto->currency = $request['currency'];

        $transactionDto = new TransactionDto();
        $transactionDto->amount = $transaction['amount'];
        $transactionDto->currency = $transaction['currency'];


        $validator = new RequestMoneyValidator();
        var_dump($validator->validate($requestDto, $transactionDto));
        return view('welcome');
    }
}
